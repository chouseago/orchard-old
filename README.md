# orchard

Orchard is a semantic mapping system which handles multiple observational passes through the same environment.
It maintains a collection of observed objects within the environment and smoothly handles them changing their position between
environmnetal passes.

This repository also contains multiple tools for handling and inspecting object models.

## Building
Build me with catkin - will build supereight as well

	catkin_make

## Maintainer
Charlie Houseago - c.houseago16@imperial.ac.uk
