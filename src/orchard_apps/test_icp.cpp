/******************************************************************************
*
* Header stuff goes in here
*
******************************************************************************/

// INCLUDES
// ===========================================================================
#include <se/ObjectMappingSystem.h>
#include <se/object.h>
#include <se/preprocessing.h>
#include <default_parameters.h>
#include <interface.h>
#include <perfstats.h>

#include "se/icp/ConfigMap.hpp"

#include <cstring>
#include <iostream>

#include <boost/thread.hpp>
#include <boost/program_options.hpp>

#include <se/orchard/Implementation/geometry_utils.hpp>
// ===========================================================================

PerfStats Stats;

// Define Command Line Variables
// ===========================================================================
std::string dirname;
int frame_number;
int object_number;
// ===========================================================================

// Define Static Objects
// ===========================================================================
// static ObjectMappingSystem *pipeline = NULL;
static Eigen::Vector3f init_pose;
static DepthReader *reader = NULL;
static ObjectMappingSystem *pipeline = NULL;

uint16_t * inputDepth = NULL;
static uchar3 * inputRGB = NULL;

// ===========================================================================

// Set up namespace for boost program options
namespace
{
    const size_t ERROR_IN_COMMAND_LINE = 1;
    const size_t SUCCESS = 0;
    const size_t ERROR_UNHANDLED_EXCEPTION = 2;

} // namespace

DepthReader *createReader(Configuration *config, std::string filename = "");
void readConfig(string FName, GoICP & goicp);
Eigen::Matrix<unsigned char, 3, 1> * toEigen(uchar3 * in, uint2 inputSize);

int main(int argc, char ** argv) {
    std::cout << "Hello World" << std::endl;

    // ===========================================================================
    // Parse Command Line Options
    // ===========================================================================
    std::cout << "Parsing Command Line Options" << std::endl;
    try{
        // =========================================================================
        // Initialise things
        // =========================================================================
        int argc_config; // TODO: Read from file
        char** argv_config; // TODO: Read from file

        Configuration config = parseArgs(argc, argv);
        config.input_file = "/media/charlie/HDD/datasets/tum/rgbd_dataset_freiburg1_desk/scene.raw";
        config.groundtruth_file = "/media/charlie/HDD/datasets/tum/rgbd_dataset_freiburg1_desk/scene.raw.txt";

        reader = createReader(&config);

        uint2 inputSize = make_uint2(224, 172); // TODO: Read from file
        const uint2 computationSize = make_uint2(
                inputSize.x / config.compute_size_ratio,
                inputSize.y / config.compute_size_ratio);

        Eigen::Vector4f camera =
                (reader != NULL) ? reader->getK() : Eigen::Vector4f::Constant(0.0f);
        camera /= config.compute_size_ratio;

        camera = { 177.484, // f_x
                   241.917, // f_y
                   113.294, // c_x
                   85.1316, // c_y
        };

        init_pose = config.initial_pos_factor.cwiseProduct(config.volume_size);

        inputDepth =
                (uint16_t*) malloc(sizeof(uint16_t) * inputSize.x*inputSize.y);
        inputRGB =
                (uchar3*) malloc(sizeof(uchar3) * inputSize.x * inputSize.y);

        // May not be needed, here for later
        pipeline = new ObjectMappingSystem(
                Eigen::Vector2i(computationSize.x, computationSize.y),
                Eigen::Vector3i::Constant(static_cast<int>(config.volume_resolution.x())),
                Eigen::Vector3f::Constant(config.volume_size.x()),
                init_pose,
                config.pyramid, config);

        // Test Loading of Objects
        const std::string object_filename = "/media/charlie/HDD/Maps/eval3/reference/Object12";
        const std::string keyframe_dir = "/media/charlie/HDD/Maps/eval3/303_raw/Object16/keyframe0";

        ObjectPointer test_object_r = std::make_shared<Object>(object_filename, Eigen::Vector2i(computationSize.x, computationSize.y));
        object_keyframe test_keyframe(keyframe_dir, computationSize.x, computationSize.y);

        // =========================================================================
        // Main Program
        // =========================================================================

        // Test the initial keyframe matches
//        instanceSeg init_detection(test_object_r->saved_keyframes[0].instance_mask_);
//        pipeline->setImages(test_object_r->saved_keyframes[0].float_rgb_, test_object_r->saved_keyframes[0].float_depth_);
//        Eigen::Matrix4f init_T_Cl_O = Eigen::Matrix4f::Identity();
//        float init_residual = pipeline->relocalizeObject(init_T_Cl_O, init_detection, test_object_r);
//        std::cout << "Residual: " << init_residual << std::endl;
//        std::cout << "Transform: " << init_T_Cl_O << std::endl;


        // Test the correct correspondence
        instanceSeg test_detection(test_keyframe.instance_mask_);
        pipeline->setImages(test_keyframe.float_rgb_, test_keyframe.float_depth_);
        Eigen::Matrix4f this_T_Cl_O = Eigen::Matrix4f::Identity();
        float residual = pipeline->relocalizeObject(this_T_Cl_O, test_detection, test_object_r);
        std::cout << "Residual: " << residual << std::endl;
        std::cout << "Transform: " << this_T_Cl_O << std::endl;

        // End main program
        std::cout << "Test finished, ending program" << std::endl;
        return 1;
    } // try
        // ===========================================================================
        // Boost Exception Handling
        // ===========================================================================
    catch(std::exception& e)
    {
        std::cerr << "Unhandled Exception reached the top of main: "
                  << e.what() << ", application will now exit" << std::endl;
        return ERROR_UNHANDLED_EXCEPTION;
    } // catch

    return SUCCESS;
    // ===========================================================================
} // main

void readConfig(string FName, GoICP & goicp)
{
    // Open and parse the associated config file
    ConfigMap config(FName.c_str());

    goicp.MSEThresh = config.getF("MSEThresh");
    goicp.initNodeRot.a = config.getF("rotMinX");
    goicp.initNodeRot.b = config.getF("rotMinY");
    goicp.initNodeRot.c = config.getF("rotMinZ");
    goicp.initNodeRot.w = config.getF("rotWidth");
    goicp.initNodeTrans.x = config.getF("transMinX");
    goicp.initNodeTrans.y = config.getF("transMinY");
    goicp.initNodeTrans.z = config.getF("transMinZ");
    goicp.initNodeTrans.w = config.getF("transWidth");
    goicp.trimFraction = config.getF("trimFraction");
    // If < 0.1% trimming specified, do no trimming
    if(goicp.trimFraction < 0.001)
    {
        goicp.doTrim = false;
    }
    goicp.dt.SIZE = config.getI("distTransSize");
    goicp.dt.expandFactor = config.getF("distTransExpandFactor");

    cout << "CONFIG:" << endl;
    config.print();
    //cout << "(doTrim)->(" << goicp.doTrim << ")" << endl;
    cout << endl;
}

// quick fix for uchar3 to rgb
Eigen::Matrix<unsigned char, 3, 1> * toEigen(uchar3 * in, uint2 inputSize)
{
    Eigen::Matrix<unsigned char, 3, 1> * out = new Eigen::Matrix<unsigned char, 3, 1>[inputSize.x*inputSize.y];

    for (unsigned int t = 0; t < inputSize.x*inputSize.y; t++) {
        (out+t)->x() = +(in+t)->x;
        (out+t)->y() = +(in+t)->y;
        (out+t)->z() = +(in+t)->z;
    }
    return out;
}
