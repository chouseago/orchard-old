/******************************************************************************
*
* Header stuff goes in here
*
******************************************************************************/

// INCLUDES
// ===========================================================================
#include <se/ObjectMappingSystem.h>
#include <se/object.h>
#include <se/preprocessing.h>
#include <default_parameters.h>
#include <interface.h>
#include <perfstats.h>

#include "se/icp/ConfigMap.hpp"

#include <cstring>
#include <iostream>

#include <boost/thread.hpp>
#include <boost/program_options.hpp>
// ===========================================================================

PerfStats Stats;

// Define Command Line Variables
// ===========================================================================
std::string dirname;
int frame_number;
int object_number;
// ===========================================================================

// Define Static Objects
// ===========================================================================
// static ObjectMappingSystem *pipeline = NULL;
static Eigen::Vector3f init_pose;
static DepthReader *reader = NULL;
static ObjectMappingSystem *pipeline = NULL;

uint16_t * inputDepth = NULL;
static uchar3 * inputRGB = NULL;

// ===========================================================================

// Set up namespace for boost program options
namespace
{
  const size_t ERROR_IN_COMMAND_LINE = 1;
  const size_t SUCCESS = 0;
  const size_t ERROR_UNHANDLED_EXCEPTION = 2;

} // namespace

DepthReader *createReader(Configuration *config, std::string filename = "");
void readConfig(string FName, GoICP & goicp);
Eigen::Matrix<unsigned char, 3, 1> * toEigen(uchar3 * in, uint2 inputSize);

int main(int argc, char ** argv) {
  std::cout << "Hello World" << std::endl;

  // ===========================================================================
  // Parse Command Line Options
  // ===========================================================================
  std::cout << "Parsing Command Line Options" << std::endl;
  try{
    // Define and Parse the Program options
    namespace po = boost::program_options;
    po::options_description desc("Options");
    desc.add_options()
      ("help,h", "Print help messages")
      ("directory,d", po::value< std::string >(&dirname), "Set Directory to read map from")
      ("frame,f", po::value<int>(&frame_number), "Set frame number")
      ("object,o", po::value<int>(&object_number), "Set object number");

    po::variables_map vm;
    try {
      po::store(po::parse_command_line(argc, argv, desc), vm); // can throw
      if ( vm.count("help") )  {
        std::cout << "Available Commands:" << std::endl << desc << std::endl;
        return SUCCESS;
      }
      po::notify(vm); // throws an error, so do after help in case of problems
    } // try
    catch(po::error& e) {
      std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
      std::cerr << desc << std::endl;
      return ERROR_IN_COMMAND_LINE;
    } // catch
    // End option parsing

    // =========================================================================
    // Initialise things
    // =========================================================================
    int argc_config; // TODO: Read from file
    char** argv_config; // TODO: Read from file

    Configuration config = parseArgs(argc_config, argv_config);
    config.input_file = "/media/charlie/HDD/datasets/tum/rgbd_dataset_freiburg1_desk/scene.raw";
    config.groundtruth_file = "/media/charlie/HDD/datasets/tum/rgbd_dataset_freiburg1_desk/scene.raw.txt";

    reader = createReader(&config);

    uint2 inputSize = make_uint2(640, 480); // TODO: Read from file
  	const uint2 computationSize = make_uint2(
  			inputSize.x / config.compute_size_ratio,
  			inputSize.y / config.compute_size_ratio);

    Eigen::Vector4f camera =
      (reader != NULL) ? reader->getK() : Eigen::Vector4f::Constant(0.0f);
    camera /= config.compute_size_ratio;

    camera = {256.575012, 256.575012, 160.0, 120.0};

    init_pose = config.initial_pos_factor.cwiseProduct(config.volume_size);

    inputDepth =
          (uint16_t*) malloc(sizeof(uint16_t) * inputSize.x*inputSize.y);
  	inputRGB =
          (uchar3*) malloc(sizeof(uchar3) * inputSize.x * inputSize.y);

    // May not be needed, here for later
    pipeline = new ObjectMappingSystem(
          Eigen::Vector2i(computationSize.x, computationSize.y),
          Eigen::Vector3i::Constant(static_cast<int>(config.volume_resolution.x())),
          Eigen::Vector3f::Constant(config.volume_size.x()),
          init_pose,
          config.pyramid, config);

    // Test Loading of Objects

    pipeline->loadLegacyObjects(camera, config.mu);

    std::cout << "Initialization works" << std::endl;

    // =========================================================================
    // Main Program
    // =========================================================================

    // Load depth and rgb images
    std::string img_filename;

    if ((reader == NULL) || (reader->cameraActive == false)) {
			std::cerr << "No valid input file specified\n";
			exit(1);
		}

    Eigen::Matrix4f gt_pose;
    // Grab the first 10 frames and discard
    for (int i = 0; i<10; i++)
      reader->readNextData(inputRGB, inputDepth, gt_pose);

    static int frame_number = 0;
    frame_number = reader->getFrameNumber();

    std::cout << "Frame: " << frame_number << std::endl;

    pipeline->preprocessing(inputDepth,
            toEigen(inputRGB, inputSize),
            Eigen::Vector2i(inputSize.x, inputSize.y),
            config.bilateralFilter);

    pipeline->setPose(gt_pose);

    // Perform Image Operations
    se::Image<POINT3D> imCloud(computationSize.x, computationSize.y);
    se::Image<float> float_depth_(computationSize.x, computationSize.y);
    mm2metersKernel(float_depth_, inputDepth, Eigen::Vector2i(inputSize.x, inputSize.y));
    const Eigen::Matrix4f& invK = getInverseCameraMatrix(camera);

    std::cout << "Running Segmentation" << std::endl;
    pipeline->runMaskRCNN(frame_number);

    std::cout << "Registering" << std::endl;
    pipeline->registerWrapper(camera, gt_pose);

    // End main program
    std::cout << "Objects Registered, ending program" << std::endl;
    return 1;
  } // try
  // ===========================================================================
  // Boost Exception Handling
  // ===========================================================================
  catch(std::exception& e)
  {
    std::cerr << "Unhandled Exception reached the top of main: "
              << e.what() << ", application will now exit" << std::endl;
    return ERROR_UNHANDLED_EXCEPTION;
  } // catch

  return SUCCESS;
  // ===========================================================================
} // main

void readConfig(string FName, GoICP & goicp)
{
	// Open and parse the associated config file
	ConfigMap config(FName.c_str());

	goicp.MSEThresh = config.getF("MSEThresh");
	goicp.initNodeRot.a = config.getF("rotMinX");
	goicp.initNodeRot.b = config.getF("rotMinY");
	goicp.initNodeRot.c = config.getF("rotMinZ");
	goicp.initNodeRot.w = config.getF("rotWidth");
	goicp.initNodeTrans.x = config.getF("transMinX");
	goicp.initNodeTrans.y = config.getF("transMinY");
	goicp.initNodeTrans.z = config.getF("transMinZ");
	goicp.initNodeTrans.w = config.getF("transWidth");
	goicp.trimFraction = config.getF("trimFraction");
	// If < 0.1% trimming specified, do no trimming
	if(goicp.trimFraction < 0.001)
	{
		goicp.doTrim = false;
	}
	goicp.dt.SIZE = config.getI("distTransSize");
	goicp.dt.expandFactor = config.getF("distTransExpandFactor");

	cout << "CONFIG:" << endl;
	config.print();
	//cout << "(doTrim)->(" << goicp.doTrim << ")" << endl;
	cout << endl;
}

// quick fix for uchar3 to rgb
Eigen::Matrix<unsigned char, 3, 1> * toEigen(uchar3 * in, uint2 inputSize)
{
    Eigen::Matrix<unsigned char, 3, 1> * out = new Eigen::Matrix<unsigned char, 3, 1>[inputSize.x*inputSize.y];

    for (unsigned int t = 0; t < inputSize.x*inputSize.y; t++) {
        (out+t)->x() = +(in+t)->x;
        (out+t)->y() = +(in+t)->y;
        (out+t)->z() = +(in+t)->z;
    }
    return out;
}
