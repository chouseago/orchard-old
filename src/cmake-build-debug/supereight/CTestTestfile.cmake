# CMake generated Testfile for 
# Source directory: /home/charlie/orchard/src/supereight
# Build directory: /home/charlie/orchard/src/cmake-build-debug/supereight
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("se_core")
subdirs("se_shared")
subdirs("se_tools")
subdirs("se_denseslam")
subdirs("se_apps")
