# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.14

# compile CXX with /usr/bin/c++
CXX_FLAGS = -g   -std=c++14 -march=native -g -fno-omit-frame-pointer -Wall -Wextra -Wno-unknown-pragmas -fopenmp

CXX_DEFINES = -DSE_FIELD_TYPE=SDF

CXX_INCLUDES = -I/usr/include/openni2 -I/home/charlie/orchard/src/supereight/se_apps/include -I/home/charlie/orchard/src/supereight/se_denseslam/include -I/usr/include/eigen3 -I/opt/ros/kinetic/include -I/opt/ros/kinetic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp -I/opt/ros/kinetic/include/opencv-3.3.1-dev -I/opt/ros/kinetic/include/opencv-3.3.1-dev/opencv -I/home/charlie/orchard/src/supereight/se_tools/. -I/home/charlie/orchard/src/supereight/se_shared/thirparty -I/home/charlie/orchard/src/supereight/se_shared/. -I/home/charlie/orchard/src/supereight/se_core/include -isystem /usr/local/include/opencv 

