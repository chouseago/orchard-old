# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/charlie/orchard/src/orchard_apps/ext/PowerMonitor.cpp" "/home/charlie/orchard/src/cmake-build-debug/orchard_apps/CMakeFiles/test_registration.dir/ext/PowerMonitor.cpp.o"
  "/home/charlie/orchard/src/orchard_apps/ext/reader.cpp" "/home/charlie/orchard/src/cmake-build-debug/orchard_apps/CMakeFiles/test_registration.dir/ext/reader.cpp.o"
  "/home/charlie/orchard/src/orchard_apps/test_registration.cpp" "/home/charlie/orchard/src/cmake-build-debug/orchard_apps/CMakeFiles/test_registration.dir/test_registration.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_PACKAGE_NAME=\"orchard_apps\""
  "SE_FIELD_TYPE=SDF"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../orchard_apps/orchardMapper"
  "/usr/include/eigen3"
  "../supereight/se_denseslam/include"
  "../supereight/se_shared"
  "../supereight/se_core/include"
  "../supereight/se_apps/include"
  "/opt/ros/kinetic/include"
  "/opt/ros/kinetic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/opt/ros/kinetic/include/opencv-3.3.1-dev"
  "/opt/ros/kinetic/include/opencv-3.3.1-dev/opencv"
  "../orchard_apps/test_registration"
  "../supereight/se_tools/."
  "../supereight/se_shared/thirparty"
  "../supereight/se_shared/."
  "/usr/local/include/opencv"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/charlie/orchard/src/cmake-build-debug/supereight/se_denseslam/CMakeFiles/se-denseslam-sdf.dir/DependInfo.cmake"
  "/home/charlie/orchard/src/cmake-build-debug/supereight/se_tools/CMakeFiles/lodepng.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
