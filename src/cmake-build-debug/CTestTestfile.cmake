# CMake generated Testfile for 
# Source directory: /home/charlie/orchard/src
# Build directory: /home/charlie/orchard/src/cmake-build-debug
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("supereight")
subdirs("orchard_apps")
subdirs("detection_msgs")
subdirs("masker")
