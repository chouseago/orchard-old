// General Includes

// OpenCV
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

// Ros includes
#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>

#include <masker/segmentor.h>

using namespace std;

int main(int argc, char** argv)
{
    // Set up segmentor
    auto network = Segmentor(argc, argv);

    // Read images from directory
    vector<cv::String> fn;
    glob("../../../test_images/*.jpg", fn, false);

    vector<cv::Mat> images;
    size_t count = fn.size(); // number of png files in target directory
    for (size_t i=0; i<count; i++)
        images.push_back(imread(fn[i], CV_LOAD_IMAGE_COLOR));

    // Send images to Segmentor one by one
    cout << "Streaming images over Ros" << endl;

    for(auto&& img: images) {
      std::vector<std::pair<cv::Mat,double>> segmentations = network.ProcessFrame(img);
      std:cout << "Returned: " << segmentations.size() << " images" << std::endl;

      int i = 0;
      for(auto&& obj: segmentations) {
          cv::namedWindow("Object Number " + std::to_string(i), cv::WINDOW_NORMAL);
          cv::imshow("Object Number " + std::to_string(i), obj.first);
          i++;
      }
      cv::waitKey(0);
      cv::destroyAllWindows();
    }

    return 0;
}
