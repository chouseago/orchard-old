#include <masker/segmentor.h>

MaskR::MaskR()
{
    // Setup Server and Client
    srvClient = nh_.serviceClient<masker::MaskImage>("/MaskR");

    // Test Service
    std::cout << "Testing Connection to Service" << std::endl;
    masker::MaskImage testSrv;
    std::cout << "Initialization Successful" << std::endl;
    return;
}

MaskR::~MaskR() {}

std::vector<std::pair<cv::Mat,double>> MaskR::Process(const cv::Mat& input) {
    masker::MaskImage srv;
    auto imgMsg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", input).toImageMsg();
    srv.request.frame = *imgMsg;

    std::vector<detection_msgs::Detection> response;
    if(srvClient.call(srv))
    {
      response = srv.response.segmentation.detections;
      std::cout << "Number of Detections received: " << response.size() << std::endl;
    }
    else
    {
      ROS_ERROR("Failed to call service MaskR");
    }

    std::vector<std::pair<cv::Mat,double>> results;
    for(int i=0; i<response.size(); i++) {
      cv_bridge::CvImagePtr cv_ptr;
      cv_ptr = cv_bridge::toCvCopy(response[i].mask, sensor_msgs::image_encodings::BGR8);

      double max_score = 0.0;
      for(int j=0; j<20; j++) {
        if(response[i].scores[j] > max_score) { max_score = response[i].scores[j]; }
      }
      results.push_back(std::make_pair(cv_ptr->image, max_score));
    }
    return results;
}

Segmentor::Segmentor(int argc, char** argv)
{
    std::cout << "Initializing Segmentor" << std::endl;
    ros::init(argc, argv, "Segmentor_client");

    masker = new MaskR();
}

Segmentor::~Segmentor() {
}

std::vector<std::pair<cv::Mat, double>> Segmentor::ProcessFrame(const cv::Mat& input) {
    std::vector<std::pair<cv::Mat, double>> result = masker->Process(input);
    return result;
}
