#!/bin/bash
# Supereight script
#echo
#echo Running supereight with different options:
#echo "File:			Command:"
#echo "Room			0"
#echo "Desk			1"
#echo "XYZ			2"
#echo "Floor         3"
#echo

#read -p 'Option: ' optionvar

#if [ $optionvar -eq 0 ]; then	filename="rgbd_dataset_freiburg1_room"; fi
#if [ $optionvar -eq 1 ]; then filename="rgbd_dataset_freiburg1_desk"; fi
#if [ $optionvar -eq 2 ]; then filename="rgbd_dataset_freiburg1_xyz"; fi
#if [ $optionvar -eq 3 ]; then filename="rgbd_dataset_freiburg1_floor"; fi

echo
echo Please enter sequence number
echo
read -p 'Number: ' optionvar

if [ $optionvar -eq 0 ]; then filename="scans/0a4b8ef6-a83a-21f2-8672-dce34dd0d7ca"; fi
if [ $optionvar -eq 1 ]; then filename="scans/0ad2d3a1-79e2-2212-9b99-a96495d9f7fe"; fi
if [ $optionvar -eq 2 ]; then filename="scans/0ad2d3a3-79e2-2212-9a51-9094be707ec2"; fi
if [ $optionvar -eq 3 ]; then filename="scans/0ad2d3a5-79e2-2212-9a9e-2502a05fa678"; fi
if [ $optionvar -eq 4 ]; then filename="scans/0ad2d3a7-79e2-2212-9a1b-8737842a24e2"; fi
if [ $optionvar -eq 5 ]; then filename="scans/0ad2d38f-79e2-2212-98d2-9b5060e5e9b5"; fi
if [ $optionvar -eq 6 ]; then filename="scans/0ad2d39b-79e2-2212-99ae-830c292cd079"; fi
if [ $optionvar -eq 7 ]; then filename="scans/0ad2d39d-79e2-2212-99aa-1e95c5b94289"; fi
if [ $optionvar -eq 8 ]; then filename="scans/0ad2d382-79e2-2212-98b3-641bf9d552c1"; fi
if [ $optionvar -eq 9 ]; then filename="scans/0ad2d384-79e2-2212-9b18-72b44eb5463f"; fi
if [ $optionvar -eq 10 ]; then filename="scans/0ad2d386-79e2-2212-9b40-43d081db442a"; fi
if [ $optionvar -eq 11 ]; then filename="scans/0ad2d389-79e2-2212-9b0a-5f0e6f4982b5"; fi
if [ $optionvar -eq 12 ]; then filename="scans/0ad2d391-79e2-2212-98a3-7e2440884395"; fi
if [ $optionvar -eq 13 ]; then filename="scans/0ad2d395-79e2-2212-9b89-83581fad7390"; fi
if [ $optionvar -eq 14 ]; then filename="scans/0ad2d399-79e2-2212-99cf-7a3512734bd7"; fi
if [ $optionvar -eq 15 ]; then filename="scans/0cac75a1-8d6f-2d13-8dab-0ecc3b317dc7"; fi
if [ $optionvar -eq 16 ]; then filename="scans/0cac75a7-8d6f-2d13-8fdc-083ff44d10fb"; fi
if [ $optionvar -eq 17 ]; then filename="scans/0cac75ab-8d6f-2d13-8fea-b1eb7e9bf6e7"; fi
if [ $optionvar -eq 18 ]; then filename="scans/0cac75ad-8d6f-2d13-8c74-5de4dfc4affc"; fi
if [ $optionvar -eq 19 ]; then filename="scans/0cac75af-8d6f-2d13-8f9e-ed3f62665aed"; fi
if [ $optionvar -eq 20 ]; then filename="scans/0cac75b1-8d6f-2d13-8c17-9099db8915bc"; fi
if [ $optionvar -eq 21 ]; then filename="scans/0cac75b3-8d6f-2d13-8f4b-3d74070429df"; fi
if [ $optionvar -eq 22 ]; then filename="scans/0cac75b7-8d6f-2d13-8cb2-0b4e06913140"; fi
if [ $optionvar -eq 23 ]; then filename="scans/0cac75b9-8d6f-2d13-8f77-315ac9a8b547"; fi
if [ $optionvar -eq 100 ]; then filename="eval1/2e369567-e133-204c-909a-c5da44bb58df"; fi
if [ $optionvar -eq 101 ]; then filename="eval1/095821f9-e2c2-2de1-9707-8f735cd1c148"; fi
if [ $optionvar -eq 102 ]; then filename="eval1/095821fb-e2c2-2de1-94df-20f2cb423bcb"; fi
if [ $optionvar -eq 200 ]; then filename="eval2/02b33e01-be2b-2d54-93fb-4145a709cec5"; fi
if [ $optionvar -eq 201 ]; then filename="eval2/02b33e03-be2b-2d54-9129-5d28efdd68fa"; fi
if [ $optionvar -eq 202 ]; then filename="eval2/fcf66d8a-622d-291c-8429-0e1109c6bb26"; fi
if [ $optionvar -eq 203 ]; then filename="eval2/fcf66d88-622d-291c-871f-699b2d063630"; fi
if [ $optionvar -eq 300 ]; then filename="eval3/095821f7-e2c2-2de1-9568-b9ce59920e29"; fi
if [ $optionvar -eq 301 ]; then filename="eval3/2e369567-e133-204c-909a-c5da44bb58df"; fi
if [ $optionvar -eq 302 ]; then filename="eval3/095821f9-e2c2-2de1-9707-8f735cd1c148"; fi
if [ $optionvar -eq 303 ]; then filename="eval3/095821fb-e2c2-2de1-94df-20f2cb423bcb"; fi
# -----------------------

# ~/orchard/build/orchard_apps/orchardMapper -i /media/charlie/HDD/datasets/tum/$dataset$filename/scene.raw -s 4 -z 1 -c 2 -r 1 -C -X -g /media/charlie/HDD/datasets/tum/$dataset$filename/scene.raw.txt

#gdb --args ~/orchard/build/orchard_apps/orchardMapper -i /media/charlie/HDD/datasets/tum/$dataset$filename/scene.raw -s 4 -z 1 -c 2 -r 1 -C -X -g /media/charlie/HDD/datasets/tum/$dataset$filename/scene.raw.txt

#gdb --args ~/orchard/build/orchard_apps/orchardMapper -i /media/charlie/HDD/3RScan/$dataset$filename/sequence -s 4 -z 1 -c 1 -r 1 -C -X -g /media/charlie/HDD/3RScan/$filename/sequence

gdb --args ~/orchard/build/orchard_apps/orchardMapper -i /media/charlie/HDD/3RScan/$dataset$filename/sequence -s 5 -z 1 -c 1 -r 1 -C -X -g /media/charlie/HDD/3RScan/$filename/sequence

# ----------------------
