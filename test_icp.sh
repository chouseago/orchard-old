#!/bin/bash
# Supereight script
echo
echo Running supereight with different options:
echo "File:			Command:"
echo "Room			0"
echo "Desk			1"
echo "XYZ			2"
echo "Floor         3"
echo

read -p 'Option: ' optionvar

if [ $optionvar -eq 0 ]; then	filename="rgbd_dataset_freiburg1_room"; fi
if [ $optionvar -eq 1 ]; then filename="rgbd_dataset_freiburg1_desk"; fi
if [ $optionvar -eq 2 ]; then filename="rgbd_dataset_freiburg1_xyz"; fi
if [ $optionvar -eq 3 ]; then filename="rgbd_dataset_freiburg1_floor"; fi

# -----------------------

# ~/orchard/build/orchard_apps/orchardMapper -i /media/charlie/HDD/datasets/tum/$dataset$filename/scene.raw -s 4 -z 1 -c 2 -r 1 -C -X -g /media/charlie/HDD/datasets/tum/$dataset$filename/scene.raw.txt


gdb --args ~/orchard/build/orchard_apps/test_icp -i /media/charlie/HDD/datasets/tum/$dataset$filename/scene.raw -s 4 -z 1 -c 1 -r 1 -C -X -g /media/charlie/HDD/datasets/tum/$dataset$filename/scene.raw.txt

# ----------------------

